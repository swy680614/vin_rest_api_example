// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express = require('express'); // call express
var app = express(); // define our app using express
var bodyParser = require('body-parser');
var mysql = require('mysql');

var connection = mysql.createConnection({
  host: '192.168.68.5',
  user: 'vin',
  password: 'u8709069',
  database: 'TEST',
});

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());


var port = process.env.PORT || 9001; // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
  res.json({
    message: 'hooray! welcome to our api!'
  });
});

router.get('/iosTest', function(req, res) {
  res.json({
    message: 'just test'
  });
});

router.get('/iosTestDelay5Sec', function(req, res) {
  setTimeout(function() {
    res.json({
      message: 'just test delay 5 sec'
    });
  }, 5000);
});

router.get('/iosTestDelay3Sec', function(req, res) {
  setTimeout(function() {
    res.json({
      message: 'just test delay 3 sec'
    });
  }, 3000);
});

router.get('/iosTestDelay2Sec', function(req, res) {
  setTimeout(function() {
    res.json({
      message: 'just test delay 2 sec'
    });
  }, 2000);
});

router.get('/example', function(req, res) {
  var data = {
    "error": 1,
    "Example": ""
  };

  connection.query("select * from example", function(err, rows, fields) {
    if (rows.length != 0) {
      data["error"] = 0;
      data["Example"] = rows;
      res.json(data);
    } else {
      data["Example"] = 'no data found';
      res.json(data);
    }
  });
});

router.post('/example', function(req, res) {
  var Age = req.body.age;
  var Name = req.body.name;
  var data = {
    "error": 1,
    "Example": ""
  };
  if (!!Age && !!Name) {
    connection.query("insert into example values (?,?)", [Age, Name], function(err, rows, fields) {
      if (!!err) {
        data["Example"] = "Error Adding data";
      } else {
        data["error"] = 0;
        data["Example"] = "Added Successfully";
        res.json(data);
      }
    });
  } else {
    data["Example"] = "Please provide all requied data!";
    res.json(data);
  }
});

router.put('/example', function(req, res) {
  var Age = req.body.age;
  var Name = req.body.name;
  var data = {
    "error": 1,
    "Example": ""
  };
  if (!!Age && !!Name) {
    connection.query("update example set age = ? where name = ?", [Age, Name], function(err, rows, fields) {
      if (!!err) {
        data["Example"] = "Error Updating data";
      } else {
        data["error"] = 0;
        data["Example"] = "Updated Successfully";
        res.json(data);
      }
    });
  } else {
    data["Example"] = "Please provide all requied data!";
    res.json(data);
  }
});

router.delete('/example', function(req, res) {
  var Name = req.body.name;
  var data = {
    "error": 1,
    "Example": ""
  };
  if (!!Name) {
    connection.query("delete from example where name = ?", [Name], function(err, rows, fields) {
      if (!!err) {
        data["Example"] = "Error Deleting data";
      } else {
        data["error"] = 0;
        data["Example"] = "Delete Successfully";
        res.json(data);
      }
    });
  } else {
    data["Example"] = "Please provide all requied data!";
    res.json(data);
  }
});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
